from app.users.models import User
from db.repository import save,commit
from uuid import uuid4

def create(data):
    return save(User(name=data['name'],fullname=data['fullname'],nickname=data['nickname'],email=data['email']))

def get():
    return User.query.all()

def get_by_id(id):
    return User.query.get(id)


def delete(id):
    User.query.filter_by(id=id).delete()
    commit()
    return 'ok'

def update(id, data):
    user = get_by_id(id)
    user.nickname = data.get('nickname')
    user.email = data.get('email')
    commit()
    return user


