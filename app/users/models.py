from db import db

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String)
    fullname = db.Column(db.String)
    nickname = db.Column(db.String)
    email = db.Column(db.String(120),unique=True)

    # def __repr__(self):
    #    return "<User(name='%s', fullname='%s', nickname='%s')>" % ( self.name, self.fullname, self.nickname,self.email)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'fullname': self.fullname,
            'nickname': self.nickname,
            'email': self.email
        }