import re

class Email:
    def __init__(self, email):
        self.email = email

    def validation(self):
        padrao = re.search(r'[\w-]+@[\w-]+\.[\w\.-]+', self.email)

        if padrao:
            return True
        else:
            return None