from flask import Flask, json

from app.users.views import app_users

from db import migrate, db


def create_app():
    app = Flask(__name__)
    app.config.from_object('settings')
    db.init_app(app)
    migrate.init_app(app, db)
    _register_blueprint(app)
    return app


def _register_blueprint(app):
    app.register_blueprint(app_users)


def get_data(code, description):
    return json.dumps({
        'code': code,
        'message': description,
    })