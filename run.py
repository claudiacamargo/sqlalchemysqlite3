# from app import create_app
# from settings import DEBUG,HOST,PORT
#
# if __name__ == '__main__':
#     app = create_app()
#
#     # Base.metadata.create_all(engine)
#     app.run(port=PORT,host=HOST,debug=DEBUG)
#
#
#
#
#     # session.add_all([
#     #     User(name='wendy', fullname='Wendy Williams', nickname='windy'),
#     #     User(name='mary', fullname='Mary Contrary', nickname='mary'),
#     #     User(name='fred', fullname='Fred Flintstone', nickname='freddy')])
#
#
from app import create_app
from settings import DEBUG, HOST, PORT

if __name__ == '__main__':
    app = create_app()
    app.run(
        host=HOST,
        port=PORT,
        debug=DEBUG
    )

